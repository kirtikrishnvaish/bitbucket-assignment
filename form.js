function formSubmit() {

    var errors = '';
    var focus = '';
    var shippingcharges = '';
    var oneday = 40;
    var twodays = 30;
    var threedays = 20;
    var fourdays = 10;
    var tax = 0;


    document.getElementById('error_messages').innerHTML = "";
    var name = document.getElementById('name').value.trim();
    var email = document.getElementById('email').value.trim();
    var phone = document.getElementById('phone').value.trim();
    var address = document.getElementById('address').value.trim();
    var city = document.getElementById('city').value.trim();
    var postcode = document.getElementById('postcode').value.trim();
    var province = document.getElementById('province').value.trim();
    var pens = document.getElementById('pens').value.trim();
    var pencils = document.getElementById('pencils').value.trim();
    var erasers = document.getElementById('erasers').value.trim();
    var delivery = document.getElementById('delivery').value.trim();

    // Writing a regular expression to validate Post Code
    // Post code format example is N2E 1A6
    var postcoderegex = /^[A-Z][0-9][A-Z]\s[0-9][A-Z][0-9]$/;

    // Writing a regular expression to validate Phone
    // Phone format example is 123-123-1234 or 123.123.1234
    // or (123)-(123)-(1234) or (123).(123).(1234) or 1231231234
    // or 123/123/1234 or (123)/123/1234
    // or (123).123.1234 or (123)-123-1234 and some more that satisfy the rules
    var phoneRegex = /^\(?(\d{3})\)?[.\-\/\s]?(\d{3})[.\-\/\s]?(\d{4})$/;

    if (name === '') {//check if name is empty
        errors += '<li>Name is required</li>';
        document.getElementById('name').focus();
        if (focus === "") {
            focus = 'name';
        }
    }
    if (email === '') {
        errors += '<li>Email is required</li>';
        if (focus === "") {
            focus = 'email';
        }
    }
    if (phone === '') {
        errors += '<li>Phone is required</li>';
        if (focus === "") {
            focus = 'phone';
        }
    } else if (!phoneRegex.test(phone)) {
        errors += '<li>Phone is not in correct format</li>';
        if (focus === "") {
            focus = 'phone';
        }
    }
    if (address === '') {
        errors += '<li>Address is required</li>';
        if (focus === "") {
            focus = 'address';
        }
    }
    if (city === '') {
        errors += '<li>city is required</li>';
        if (focus === "") {
            focus = 'city';
        }
    }
    if (postcode === '') {
        errors += '<li>Postal Code is required</li>';
        if (focus === "") {
            focus = 'postcode';
        }
    } else if (!postcoderegex.test(postcode)) {
        errors += '<li>Postal Code is not in correct format</li>';
        if (focus === "") {
            focus = 'postcode';
        }
    }
    if (province === '') {
        errors += '<li>Province is required</li>';
        if (focus === "") {
            focus = 'province';
        }
    }
    if (pens === "0" && pencils === "0" && erasers === "0") {
        errors += '<li>Select at least one Product</li>';
        if (focus === "") {
            focus = 'pens';
        }
    }
    if (errors) {
        document.getElementById('error_messages').innerHTML = errors;
        document.getElementById(focus).focus();
    } else {

        var invoice = `
        <h2>My Invoice</h2>
       <div class="invoice_data">
           <span class="invoice_label">Name</span> 
           <span class="invoice_value">: ${name}</span> 
       </div>
       <div class="invoice_data">
            <span class="invoice_label">Email</span
            <span class="invoice_value">: ${email}</span>
        </div>
       <div class="invoice_data">
            <span class="invoice_label">Phone</span
            <span class="invoice_value">: ${phone}</span>
       </div>
       <div class="invoice_data">
            <span class="invoice_label">Address</span
            <span class="invoice_value">: ${address}</span>
       </div>
       <div class="invoice_data">
            <span class="invoice_label">City</span
            <span class="invoice_value">: ${city}</span>
        </div>
       <div class="invoice_data">
            <span class="invoice_label">Postal Code</span
            <span class="invoice_value">: ${postcode}</span>
        </div>
       <div class="invoice_data">
            <span class="invoice_label">Province</span
            <span class="invoice_value">: ${province}</span>
        </div>
       `;
        var cost = 0;
        if (pens > 0) {
            cost = cost + 5 * pens;
            invoice += `
                <div class="invoice_data">
                    <span class="invoice_label">Pens each@5 CAD</span
                    <span class="invoice_value">: ${5 * pens}</span>
                </div>
                `;
        }
        if (pencils > 0) {
            cost = cost + 10 * pencils;
            invoice += `
                <div class="invoice_data">
                    <span class="invoice_label">Pencils each@10 CAD</span
                    <span class="invoice_value">: ${10 * pencils}</span>
                </div>
                `;
        }
        if (erasers > 0) {
            cost = cost + 3 * erasers;
            invoice += `
                <div class="invoice_data">
                    <span class="invoice_label">Pencils each@3 CAD</span
                    <span class="invoice_value">: ${3 * erasers}</span>
                </div>
                `;
        }

        if(province === 'Alberta') {
            tax = cost * 0.05
        } else if (province === 'British Columbia') {
            tax = cost * 0.12
        }else if (province === 'Quebec') {
            tax = cost * 0.14975
        }else if ((province === 'Ontario') || province === 'Manitoba') {
            tax = cost * 0.13
        }
        var totalCost = cost + tax;

        if (delivery === "1") {
            totalCost = totalCost + oneday;
            shippingcharges = oneday;
        } else if (delivery === "2") {
            totalCost = totalCost + twodays;
            shippingcharges = twodays;
        } else if (delivery === "3") {
            totalCost = totalCost + threedays;
            shippingcharges = threedays;
        } else if (delivery === "4") {
            totalCost = totalCost + fourdays;
            shippingcharges = fourdays;
        }
        invoice += `
        <div class="invoice_data">
             <span class="invoice_label">Taxes</span>
             <span class="invoice_value">: CAD ${tax.toFixed(2)}</span>
        </div>
        <div class="invoice_data">
             <span class="invoice_label">Deliver Time</span>
             <span class="invoice_value">: ${delivery} Day(s)</span>
        </div>  
        <div class="invoice_data">
             <span class="invoice_label">Deliver Charges</span>
             <span class="invoice_value">: CAD ${shippingcharges}</span>
        </div>   
        <div class="invoice_data">
             <span class="invoice_label">Total Cost</span>
             <span class="invoice_value">: CAD ${totalCost}</span>
        </div>
       `;
        document.getElementById('assignment_invoice').innerHTML = invoice
    }
    return false;
}
